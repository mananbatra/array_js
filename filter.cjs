function filter(array, callBack)
{
    const filteredArray = [];
  
    for (let index in array) 
    {
      let check=callBack(array[index],index,array);//stored cb value to variable check
      if (check===true)
      {
        filteredArray.push(array[index]);
      }
    }
  
    return filteredArray;
}

module.exports=filter;