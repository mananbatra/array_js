function find(array, callBack) 
{

   if(Array.isArray(array))
   { 
    for (let key in array) 
    {
      if (callBack(array[key]))
      {
        return array[key];
      }
    }
   } 
    return undefined;
}

module.exports=find;