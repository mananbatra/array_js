function map(array, callBack) 
{
    const newArray = [];

    if(Array.isArray(array))// check passed argument is array
    {    
        for (let index=0 ;index<array.length;index++) 
        {
            newArray.push(callBack(array[index],index,array));
            //console.log(index);
        }
    }
    return newArray;
}

module.exports=map;