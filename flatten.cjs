function flatten(array,depth) 
{
    const flattenedArray = [];
    if(depth===undefined)
    {
      depth=1;
    }
  
    function flattenArray(elements,depthCheck) 
    {
      for (let key in elements) 
      {
        if (Array.isArray(elements[key]) && depthCheck>0) 
        {
          flattenArray(elements[key],depthCheck-1); // To Recursively flatten nested array repeatedly
        } else 
        {
          flattenedArray.push(elements[key]); // Add non-array elements to the flattened array
        }
      }
    }
  
    flattenArray(array,depth);
    return flattenedArray;
}

  module.exports=flatten;
  
  