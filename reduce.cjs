function reduce(array, callBack, startValue) 
{
    let result=0;
    if(Array.isArray(array))
    {
        result = startValue;
  
        if (result === undefined) 
        {
            if (array.length === 0) 
            {
                return 0;
            }

            result = array[0];

            for (let index=1 ;index<array.length;index++) 
            {
             result = callBack(result, array[index],index,array);
            }
        } else 
        {
            for (let index in array)
            {
                result = callBack(result, array[index],index,array);
            }
        }
    }
    return result;
}

module.exports=reduce;
